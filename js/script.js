"use strict";

let width = 620; // ширина слайда
let count = 1; // количество слайдов
let slider = document.getElementById("slider");
let widhtSlider = slider.offsetWidth;
let list = slider.querySelector('ul');
let listElems = list.getElementsByClassName('slide');
let position = 0; // текущий сдвиг влево
let sliderWidth = width * listElems.length;

list.style.width = sliderWidth + "px"; // присвоение ширины слайдеру равному ширине всех блоков

slider.querySelector('.prev').onclick = function() {
  // сдвиг влево
  // последнее передвижение влево может быть не на 3, а на 2 или 1 элемент
  position = Math.min(position + width * count, 0);
  list.style.marginLeft = position + 'px';
};

slider.querySelector('.next').onclick = function() {
  // сдвиг вправо
  // последнее передвижение вправо может быть не на 3, а на 2 или 1 элемент
    position = Math.max(position - width * count, -width * (listElems.length - count));

    if (position < sliderWidth ) {
      list.style.marginLeft = position + 'px';
    } if (position < -1240) {
    position = -(sliderWidth - widhtSlider);
    list.style.marginLeft = position + 'px';
  }
};